package com.zhongdaima.netty.analysis.pipeline;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 欢迎关注公众号“种代码“，获取博主微信深入交流
 *
 * @author wangjianxin
 */
public class ClientBoot {
    public static void main(String[] args) {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup(1);
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast("A", new AInboundHandler());
                            ch.pipeline().addLast("B", new BOutboundHandler());
                            ch.pipeline().addLast("C", new CDuplexHandler());
                        }
                    });
            ChannelFuture channelFuture = bootstrap.connect("127.0.0.1", 8000);
            Channel channel = channelFuture.syncUninterruptibly().channel();
            channel.write(Unpooled.wrappedBuffer("Hello, 种代码".getBytes()));
            channel.flush();
            channel.closeFuture().awaitUninterruptibly();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}
