package com.zhongdaima.netty.analysis.part0.jdk.bio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * 欢迎关注公众号“种代码“，获取博主微信深入交流
 *
 * @author wangjianxin
 */
public class HelloBioClient {
    //创建多少个客户端
    private static final int CLIENTS = 2;

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < CLIENTS; i++) {
            final int clientIndex = i;
            Thread client = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //创建socket
                        Socket socket = new Socket();
                        //连接8000端口
                        socket.connect(new InetSocketAddress(8000));
                        while (true) {
                            OutputStream outputStream = socket.getOutputStream();
                            //向服务端发送”zhongdaima" + 客户端编号
                            outputStream.write(("zhongdaima" + clientIndex).getBytes());
                            InputStream inputStream = socket.getInputStream();
                            byte[] buffer = new byte[1024];
                            int readLength = inputStream.read(buffer);
                            //打印服务端返回数据
                            System.out.println(new String(buffer, 0, readLength));
                            Thread.sleep(1000);
                        }
                    } catch (Throwable e) {

                    }
                }
            });
            client.start();
        }

    }
}
