package com.zhongdaima.netty.analysis.part0.jdk.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;

/**
 * 欢迎关注公众号“种代码“，获取博主微信深入交流
 *
 * @author wangjianxin
 */
public class HelloNioServer {
    public static void main(String[] args) throws IOException {
        //创建ServerSocketChannel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //设置Channel为非阻塞
        serverSocketChannel.configureBlocking(false);
        //绑定到8000端口
        serverSocketChannel.bind(new InetSocketAddress(8000));
        //交给Connector
        new NioServerConnector(serverSocketChannel).start();
    }
}
