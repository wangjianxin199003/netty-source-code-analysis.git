package com.zhongdaima.netty.analysis.part0.jdk.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 欢迎关注公众号“种代码“，获取博主微信深入交流
 *
 * @author wangjianxin
 */
public class BioServerConnector {
    private final ServerSocket serverSocket;

    public BioServerConnector(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public void start() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Socket newSocket = null;
                    try {
                        //阻塞在这里等待新连接，返回值为一条新的连接
                        newSocket = serverSocket.accept();
                    } catch (IOException e) {
                    }
                    //将新连接交给handler处理
                    new BioServerHandler(newSocket).start();
                }
            }
        });
        thread.start();
    }


}
