package com.zhongdaima.netty.analysis.part0.jdk.bio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

/**
 * 欢迎关注公众号“种代码“，获取博主微信深入交流
 *
 * @author wangjianxin
 */
public class HelloBioServer {
    public static void main(String[] args) throws IOException {
        //创建ServerSocket
        ServerSocket serverSocket = new ServerSocket();
        //绑定到8000端口
        serverSocket.bind(new InetSocketAddress(8000));
        new BioServerConnector(serverSocket).start();
    }
}
